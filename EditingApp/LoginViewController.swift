//
//  LoginViewController.swift
//  EditingApp
//
//  Created by mrinal khullar on 5/29/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController , UITextFieldDelegate
{

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!

    
    @IBAction func loginButton(sender: AnyObject)
    {
        
    }
    
    @IBAction func forgotPasswordButton(sender: AnyObject)
    {
        
    }
    
    
    @IBAction func signUpButton(sender: AnyObject)
    {
        
    }
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        userNameField.becomeFirstResponder()
        
       
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
      textField .resignFirstResponder()
        return true
    }
    
    /*@IBAction func hideKeyPadButton(sender: AnyObject)
    {
       userNameField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }*/

    //func textField(textField: UITextField, should)
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
